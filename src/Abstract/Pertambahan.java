package Abstract;

class Pertambahan extends Kalkulator {
    public double operanA;
    public double operanB;

    @Override
    void setOperan(double operand1, double operand2) {
        this.operanA = operand1;
        this.operanB = operand2;
    }
    
    @Override
    public double hitung() {
        return operanA + operanB;
    }
}