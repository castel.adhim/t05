package Interface;

/*
 * Jangan ubah kode didalam kelas Main ini
 * Lakukan modifikasi kode pada kelas pertambahan dan pengurangan
 * Dalam konteks kalkulator, pada kondisi apa interface akan lebih cocok digunakan daripada abstract class?
 * "Jawab disini..."
 * Interface digunakan jika pada saat diantara banyak class ada yang memiliki perbedaan pada methodnya, jadi
   semua class tidak harus menggunakan implementasi ini, hanya digunakan pada yang memerlukan.
 */

import java.util.*;

public class Main {   
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukan operan 1: ");
        double operan1 = scanner.nextDouble();

        System.out.print("Masukan operan 2: ");
        double operan2 = scanner.nextDouble();

        System.out.print("Masukan operator (+ atau -): ");
        String operator = scanner.next();
        scanner.close();

        Kalkulator kalkulator;
        if (operator.equals("+")) {
            kalkulator = new Pertambahan();
        } else if (operator.equals("-")) {
            kalkulator = new Pengurangan();
        } else {
            System.out.println("Operator tidak valid!");
            return;
        }

        double result = kalkulator.hitung(operan1, operan2);
        System.out.println("Result: " + result);
    }
}