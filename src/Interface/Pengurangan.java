package Interface;

class Pengurangan implements Kalkulator {
//override untuk pengurangan
    @Override
    public double hitung(double operan1, double operan2) {
        return operan1 - operan2;
    }
}